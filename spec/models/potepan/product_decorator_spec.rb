require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "関連商品のテスト" do
    subject { taxon_a_product.related_products(LIMIT_NUM_OF_RELATED_PRODUCTS) }

    let(:taxon_a) { create(:taxon) }
    let(:taxon_a_product) { create(:product, taxons: [taxon_a]) }
    let!(:taxon_a_product1) { create(:product, taxons: [taxon_a]) }
    let!(:taxon_a_product2) { create(:product, taxons: [taxon_a]) }

    let(:taxon_b) { create(:taxon) }
    let!(:taxon_b_product1) { create(:product, taxons: [taxon_b]) }

    context "関連商品が4件以内の場合" do
      it "同じカテゴリーの商品を全て取得" do
        is_expected.to match_array [taxon_a_product1, taxon_a_product2]
      end
    end

    context "関連商品が5件以上の場合" do
      let!(:related_products) { create_list(:product, 10, taxons: [taxon_a]) }

      it "同じカテゴリーの商品を4件に絞って取得" do
        expect(subject.size).to eq LIMIT_NUM_OF_RELATED_PRODUCTS
      end
    end
  end
end
