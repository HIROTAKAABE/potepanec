require 'rails_helper'

RSpec.describe "PotepanProducts", type: :system do
  describe "商品詳細ページのテスト" do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon], name: "RUBY ON RAILS MUG") }
    let!(:related_product) { create(:product, taxons: [taxon], name: "RUBY ON RAILS BAG") }
    let!(:lonely_product)  { create(:product, name: "LONELY HAT") }

    context "任意のカテゴリーに属する商品ページの場合(関連商品が3つ以下)" do
      before do
        visit potepan_product_path(product.id)
      end

      it "商品詳細ページの表示" do
        # ページ内ヘッダーのリンクと表示
        within '.pageHeader' do
          expect(page).to have_link "Home"
          expect(page).to have_selector "h2", text: product.name
          expect(page).to have_selector "li.active", text: product.name
        end

        # 関連商品の表示と当該商品の非表示になること
        within '.productBox' do
          expect(page).to have_content related_product.name
          expect(page).to have_no_content product.name
        end
      end

      it "関連商品のリンクが、商品の詳細ページになっていること" do
        within '.productBox' do
          expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
        end
      end

      it "「一覧ページへ戻る」のリンクが、カテゴリー一覧ページになっていること" do
        within '.media-body' do
          expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
        end
      end
    end

    context "任意のカテゴリーに属する商品ページの場合(関連商品が4つ以上)" do
      let!(:related_products) { create_list(:product, 10, taxons: [taxon]) }

      before do
        visit potepan_product_path(product.id)
      end

      it "関連商品の表示が4件になっていること" do
        expect(page).to have_css '.productBox', count: LIMIT_NUM_OF_RELATED_PRODUCTS
      end
    end

    context "いかなるカテゴリーにも属さない商品ページの場合" do
      before do
        visit potepan_product_path(lonely_product.id)
      end

      it "関連商品欄が非表示であること" do
        expect(page).not_to have_selector 'h4', text: '関連商品'
        expect(page).to have_no_css '.productBox'
      end

      it "「一覧ページへ戻る」のリンクが、トップページになっていること" do
        within '.media-body' do
          expect(page).to have_link '一覧ページへ戻る', href: potepan_path
        end
      end
    end
  end
end
