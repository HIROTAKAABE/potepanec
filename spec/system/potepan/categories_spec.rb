require 'rails_helper'

RSpec.describe "PotepanCategories", type: :system do
  describe "カテゴリー別ページのテスト" do
    let!(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let!(:taxon1) { taxonomy.root.children.create(name: 'Bags') }
    let!(:taxon2) { taxonomy.root.children.create(name: 'Mugs') }
    let!(:product1) { create(:product, name: 'Ruby On Rails Tote', taxons: [taxon1]) }
    let!(:product2) { create(:product, name: 'Ruby On Rails Mug', taxons: [taxon2]) }

    before do
      visit potepan_category_path(taxon1.id)
    end

    it "ページ内ヘッダーのリンクと表示" do
      within '.pageHeader' do
        expect(page).to have_link "Home"
        expect(page).to have_link "Shop"
        expect(page).to have_selector "h2", text: taxon1.name
        expect(page).to have_selector "li.active", text: taxon1.name
      end
    end

    it "商品一覧での商品名と価格の表示" do
      within '.productCaption' do
        expect(page).to have_selector 'h5', text: product1.name
        expect(page).to have_selector 'h3', text: product1.display_price
      end
    end

    it "商品ごとのリンクを確認" do
      within '.productBox' do
        expect(page).to have_link product1.name, href: potepan_product_path(product1.id)
      end
    end

    it "カテゴリーリストでのtaxonomy,taxon名の表示" do
      within ".categoryList" do
        expect(page).to have_link taxonomy.name
        expect(page).to have_link taxon1.name
        expect(page).to have_link taxon2.name
      end
    end

    it "カテゴリーリストでのtaxon名とリンクの一致" do
      within '.productCaption' do
        expect(page).not_to have_selector 'h5', text: product2.name
      end

      within '.collapseItem' do
        click_link taxon2.name
      end

      expect(current_path).to eq potepan_category_path(taxon2.id)

      within '.productCaption' do
        expect(page).not_to have_selector 'h5', text: product1.name
        expect(page).to have_selector 'h5', text: product2.name
        expect(page).to have_selector 'h3', text: product2.display_price
      end
    end
  end
end
