require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_titleメソッドのテスト' do
    it "page_titleが与えられると、page_title - BASE_TITLEで表示" do
      expect(full_title(page_title: "Sample")).to eq "Sample - #{BASE_TITLE}"
    end

    it "page_titleが与えられないと、BASE_TITLEのみを表示" do
      expect(full_title(page_title: "")).to eq BASE_TITLE
    end

    it "page_titleがnilだと、BASE_TITLEのみを表示" do
      expect(full_title(page_title: nil)).to eq BASE_TITLE
    end

    it "page_titleが引数で与えられないと、BASE_TITLEのみを表示" do
      expect(full_title).to eq BASE_TITLE
    end
  end
end
