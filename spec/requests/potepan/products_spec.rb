require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "商品詳細ページ(showアクション)のテスト" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "商品ページの表示" do
      expect(response).to have_http_status(200)
    end

    it "商品名の表示" do
      expect(response.body).to include product.name
    end

    it "価格の表示" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品紹介文の表示" do
      expect(response.body).to include product.description
    end

    it "関連商品の表示" do
      expect(response.body).to include related_product.name
      expect(response.body).to include related_product.display_price.to_s
    end
  end
end
