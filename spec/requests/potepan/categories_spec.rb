require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "カテゴリー別ページ(showアクション)のテスト" do
    let(:taxonomy) { create(:taxonomy, taxons: [taxon]) }
    let(:taxon) { create(:taxon, products: [product]) }
    let(:product) { create(:product) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "カテゴリー別ページの表示" do
      expect(response).to have_http_status(200)
    end

    it "taxonmy名の表示(商品カテゴリー)" do
      expect(response.body).to include taxonomy.name
    end

    it "taxon名の表示(商品カテゴリー/ドロップダウン)" do
      expect(response.body).to include taxon.name
    end

    it "カテゴリーごとの商品数の表示" do
      expect(response.body).to include taxon.products.count.to_s
    end

    it "product名の表示" do
      expect(response.body).to include product.name
    end
  end
end
