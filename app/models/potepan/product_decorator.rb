module Potepan::ProductDecorator
  def related_products(limit_num_of_related_products)
    Spree::Product.includes(master: [:default_price, :images]).in_taxons(taxons).
      where.not(id: id).distinct.order("RAND()").limit(limit_num_of_related_products)
  end

  Spree::Product.prepend self
end
