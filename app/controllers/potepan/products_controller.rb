class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.friendly.find(params[:id])
    @related_products = @product.related_products(LIMIT_NUM_OF_RELATED_PRODUCTS) if @product.taxons.present?
  end
end
